-- class table
local Terrain = {}
-- Terrain.face_order = {"top", "left", "bottom", "forward", "right", "back"}

function Terrain.init(arg_table)
	-- Terrain.tile_sheet = assets.images.tileset2
	Terrain.tile_sheet = arg_table.tile_sheet
end

-- created so that instances of Terrain use the class's functions (like :draw, if I had hadded that in this file)
local instanceMetatable = {
	__index = Terrain
}

function Terrain.new(Terrain, settings)
	if settings then
		assert(type(settings) == 'table', "Terrain constructor requires argument to be settings table")
	end
	local instance = settings
	instance.color = instance.color or {1, 1, 1}
	instance.rows = instance.rows or 8
	instance.cols = instance.cols or 8
	instance.heights = instance.heights or {1, 1.25, 1.5}
	
	-- set tilesheet info (.tiles table property)
	instance.tiles = {
		sheet = Terrain.tile_sheet
	}
	instance.tiles.width, instance.tiles.height = instance.tiles.sheet:getDimensions()
	instance.tiles.rows, instance.tiles.cols = 4, 4		-- # rows and columns rendered in tilesheet
	instance.tiles.cell_width, instance.tiles.cell_height = instance.tiles.width / instance.tiles.cols, instance.tiles.height / instance.tiles.rows
	instance.tiles.uv_width, instance.tiles.uv_height = 1/instance.tiles.cols, 1/instance.tiles.rows
	
	setmetatable(instance, instanceMetatable)
	
	instance:makeMaps()
	instance.model = instance:makeModel()
	instance:makeEvents()
	
	return instance
end

function Terrain:draw()
	if self.model then
		lg.setColor(self.color)
		self.model:draw(self.shader)
	end
end

function Terrain:listen(bool)
	for name, event in pairs(self.events) do
		if bool then
			event:register()
		else
			event:remove()
		end
	end
end

function Terrain:makeMaps()
	local cells = {}
	local rows, cols = self.rows, self.cols
	
	local noise_offset = love.math.random() * 0.923853749
	for i = 1, rows do
		cells[i] = {}
		for j = 1, cols do
			cells[i][j] = {}
			local cell = cells[i][j]
			cell.row, cell.col = i, j
			
			-- local n1 = love.math.noise(i * 0.0250105 + noise_offset, j * 0.0250205 + noise_offset)
			local n2 = love.math.noise(j * 0.20105 + noise_offset, i * 0.30104 + noise_offset)
			local n3 = love.math.noise(i * 2.0105 + noise_offset, j * 2.0104 + noise_offset)
			
			local final_noise
			-- final_noise = (n1 * 3 + n2 * 1 + n3 * 0.25) / 4.25
			-- final_noise = n1 * 0.50 + n2 * 0.50
			-- final_noise = n1
			-- final_noise = n2
			local flatness = 0.3 + 0.1 * love.math.random()
			final_noise = n3 * (1 - flatness) + n2 * flatness
			
			-- cells[i][j].height = math.round(final_noise * (z_max - z_min)) * self.post_noise_height_multiplier + z_min
			local heights_index = math.round(final_noise * (#self.heights - 1) + 1)
			cell.height = self.heights[heights_index]
			-- print(string.format("cell.height (%s, %s): %s", i, j, cell.height))
			cell.top_tile, cell.side_tile, cell.btm_tile = 6, 9, 15
		end
	end
	
	self.cells = cells
end

function Terrain:makeModel()
	self.verts = {}
	local verts, rows, cols = self.verts, self.rows, self.cols
	
	local top_u, top_v, side_u, side_v, btm_u, btm_v
	
	for i = 0, rows - 1 do
		for j = 0, cols - 1 do
			-- add 6*6 vertices per cell
			-- face order: z+, x-, z-, y+, x+, y-
			-- vert order: 1, 2, 3 / 2, 3, 4
			--[[ vertex attributes:
				x, y, z, u, v, nx, ny, nz, r, g, b, a
			]]
			
			local cell = self.cells[i+1][j+1]
			cell.svo = #verts
			top_u, top_v = self:getTileUV(self:getTileCoords(cell.top_tile))
			-- top_u, top_v = self:getTileUV(self:getTileCoords(3))
			side_u, side_v = self:getTileUV(self:getTileCoords(cell.side_tile))
			-- side_u, side_v = self:getTileUV(self:getTileCoords(6))
			btm_u, btm_v = self:getTileUV(self:getTileCoords(cell.btm_tile))
			-- btm_u, btm_v = self:getTileUV(self:getTileCoords(9))
			-- print(string.format("top u,v side u,v btm u,v: (%s, %s), (%s, %s), (%s, %s)", top_u, top_v, side_u, side_v, btm_u, btm_v))
			
			local uv_width, uv_height = self.tiles.uv_width, self.tiles.uv_height
			local k = cell.height
			local block_height = k	-- 1 or k
			
			-- z+ face verts
			table.insert(verts, {i, j, k, top_u, top_v})
			table.insert(verts, {i+1, j, k, top_u + uv_width, top_v})
			table.insert(verts, {i, j+1, k, top_u, top_v + uv_height})
			table.insert(verts, {i+1, j, k, top_u + uv_width, top_v})
			table.insert(verts, {i, j+1, k, top_u, top_v + uv_height})
			table.insert(verts, {i+1, j+1, k, top_u + uv_width, top_v + uv_height})
			
			-- x- face verts
			table.insert(verts, {i, j, k, side_u, side_v})
			table.insert(verts, {i, j+1, k, side_u + uv_width, side_v})
			table.insert(verts, {i, j, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i, j+1, k, side_u + uv_width, side_v})
			table.insert(verts, {i, j, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i, j+1, k-block_height, side_u + uv_width, side_v + uv_height})
			
			-- z- face verts
			table.insert(verts, {i, j, k-block_height, btm_u, btm_v})
			table.insert(verts, {i, j+1, k-block_height, btm_u + uv_width, btm_v})
			table.insert(verts, {i+1, j, k-block_height, btm_u, btm_v + uv_height})
			table.insert(verts, {i, j+1, k-block_height, btm_u + uv_width, btm_v})
			table.insert(verts, {i+1, j, k-block_height, btm_u, btm_v + uv_height})
			table.insert(verts, {i+1, j+1, k-block_height, btm_u + uv_width, btm_v + uv_height})
			
			-- y+ face verts
			table.insert(verts, {i, j+1, k, side_u, side_v})
			table.insert(verts, {i+1, j+1, k, side_u + uv_width, side_v})
			table.insert(verts, {i, j+1, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i+1, j+1, k, side_u + uv_width, side_v})
			table.insert(verts, {i, j+1, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i+1, j+1, k-block_height, side_u + uv_width, side_v + uv_height})
			
			-- x+ face verts
			table.insert(verts, {i+1, j, k, side_u, side_v})
			table.insert(verts, {i+1, j+1, k, side_u + uv_width, side_v})
			table.insert(verts, {i+1, j, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i+1, j+1, k, side_u + uv_width, side_v})
			table.insert(verts, {i+1, j, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i+1, j+1, k-block_height, side_u + uv_width, side_v + uv_height})
			
			-- y- face verts
			table.insert(verts, {i, j, k, side_u, side_v})
			table.insert(verts, {i+1, j, k, side_u + uv_width, side_v})
			table.insert(verts, {i, j, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i+1, j, k, side_u + uv_width, side_v})
			table.insert(verts, {i, j, k-block_height, side_u, side_v + uv_height})
			table.insert(verts, {i+1, j, k-block_height, side_u + uv_width, side_v + uv_height})
		end
	end
	
	-- local model = g3d.newModelFromMesh(mesh, verts)
	local model = g3d.newModel(verts, self.tiles.sheet)
	
	-- making normals with g3d doesn't respect vertexMap order
	model:makeNormals()
	
	-- model.vertexMap = vertexMap
	
	return model
end

function Terrain:makeEvents()
	assert(not self.events, "terrain.events already exists")
	self.events = {}
	self.events.mouse_released = events("mousereleased", function(x, y, button, istouch, presses)
		if self.on_mouse_released then
			self.on_mouse_released()
			-- print('called terrain.on_mouse_released()')
		end
	end)
end

function Terrain:getHeight(row, col)
	local cells = self.cells
	if cells[row] and cells[row][col] then
		return cells[row][col].height
	end
end

function Terrain:pickCell(pos_x, pos_y, pos_z, dir_x, dir_y, dir_z)
	-- if player clicked a cell, return row, col, else return nil
	
	-- first check pick ray against total terrain bounding volume aabb
	local contact_x, contact_y, contact_z, contact_length = math.collision.rayIntersectsAABB(
		pos_x, pos_y, pos_z,
		dir_x, dir_y, dir_z,
		0, 0, 0,
		self.cols, self.rows, self.heights[#self.heights]
	)
	
	if not contact_length then
		-- ray never intersects bounding volume - no cell clicked
		-- print('no contact_length')
		return
	end
	
	--[[ strategy 3:
		check every cell and keep closest contact
	]]
	local min_ct = math.huge
	local min_row, min_col
	for j = 1, self.cols do
		for i = 1, self.rows do
			local ht = self:getHeight(i, j)
			if ht then
				local cx, cy, cz, ct = math.collision.rayIntersectsAABB(
					pos_x, pos_y, pos_z,
					dir_x, dir_y, dir_z,
					i-1, j-1, 0,
					i, j, ht
				)
				if ct and (not min_row or min_ct > ct) then
					min_row, min_col, min_ct = i, j, ct
				end
			end
		end
	end
	
	if min_row and min_col then
		if self.cells[min_row] and self.cells[min_row][min_col] then
			return self.cells[min_row][min_col]
		end
	end
end

function Terrain:forEachCell(cell_callback)
	for i = 1, self.rows do
		for j = 1, self.cols do
			cell_callback(self.cells[i][j])
		end
	end
	self.must_update = true
end

function Terrain:getCell(row, col)
	if self.cells[row] then
		local cell = self.cells[row][col]
		if cell then
			return cell
		end
	end
end

function Terrain:getCellVertices(cell, face)
	local verts = self.model.verts
	local svo
	if face then
		assert(type(face) == "number", "face arg must be a number")
		assert(face >= 1 and face <= 6, "face must be between 1 and 6")
		svo = cell.svo + (face - 1) * 6
		return verts[svo + 1], verts[svo + 2], verts[svo + 3], verts[svo + 4], verts[svo + 5], verts[svo + 6]
	else
		svo = cell.svo
		return verts[svo + 1], verts[svo + 2], verts[svo + 3], verts[svo + 4], verts[svo + 5], verts[svo + 6],
			verts[svo + 7], verts[svo + 8], verts[svo + 9], verts[svo + 10], verts[svo + 11], verts[svo + 12],
			verts[svo + 13], verts[svo + 14], verts[svo + 15], verts[svo + 16], verts[svo + 17], verts[svo + 18],
			verts[svo + 19], verts[svo + 20], verts[svo + 21], verts[svo + 22], verts[svo + 23], verts[svo + 24],
			verts[svo + 25], verts[svo + 26], verts[svo + 27], verts[svo + 28], verts[svo + 29], verts[svo + 30],
			verts[svo + 31], verts[svo + 32], verts[svo + 33], verts[svo + 34], verts[svo + 35], verts[svo + 36]
	end
end

function Terrain:setCellColor(cell, colorizer)
	-- colorizer can be a color table or function that gets 1-36 and returns r, g, b, a
	local v = {}
	v[1], v[2], v[3], v[4], v[5], v[6],
	v[7], v[8], v[9], v[10], v[11], v[12],
	v[13], v[14], v[15], v[16], v[17], v[18],
	v[19], v[20], v[21], v[22], v[23], v[24],
	v[25], v[26], v[27], v[28], v[29], v[30],
	v[31], v[32], v[33], v[34], v[35], v[36] = self:getCellVertices(cell)
	
	local r, g, b, a = colorizer[1] or 1, colorizer[2] or 1, colorizer[3] or 1, colorizer[4] or 1
	for i = 1, 36 do
		if (type(colorizer) == "function") then
			v[i][9], v[i][10], v[i][11], v[i][12] = colorizer(i)
		else
			v[i][9], v[i][10], v[i][11], v[i][12] = r, g, b, a
		end
	end
	
	self.must_update = true
end

function Terrain:setCellFaceTexture(cell, face, terrain_index)
	-- error checking
	assert(type(terrain_index) == "number", "terrain_index has to be an integer, was type: " .. type(terrain_index))
	assert(math.floor(terrain_index) == terrain_index, "terrain_index not an integer: " .. tostring(terrain_index))
	
	local max_index = self.tiles.rows * self.tiles.cols
	assert(terrain_index >= 1 and terrain_index <= max_index, string.format("terrain_index out of bounds [%s - %s]: %s", 1, max_index, terrain_index))
	
	-- get local vars and set cell values
	local tile_row, tile_col = self:getTileCoords(terrain_index)
	local u, v = self:getTileUV(tile_row, tile_col)
	cell.terrain_index = terrain_index
	cell.tile_row, cell.tile_col = tile_row, tile_col
	cell.u, cell.v = u, v
	
	local uv_w, uv_h = self.tiles.uv_width, self.tiles.uv_height
	local v1, v2, v3, v4, v5, v6 = self:getCellVertices(cell, face)
	v1[4], v1[5] = u, v
	v2[4], v2[5] = u+uv_w, v
	v3[4], v3[5] = u, v+uv_h
	v4[4], v4[5] = u+uv_w, v
	v5[4], v5[5] = u, v+uv_h
	v6[4], v6[5] = u+uv_w, v+uv_h
	
	self.must_update = true
end

function Terrain:getTileCoords(index)
	local tile_row = math.round(index / self.tiles.rows)
	local tile_col = (index - 1) % self.tiles.cols + 1
	return tile_row, tile_col
end

function Terrain:getTileUV(row, col)
	-- local tiles = self.tiles.sheet
	-- local sheet_w, sheet_h = self.tiles.width, self.tiles.height
	-- local x = (row - 1) / self.tiles.rows * sheet_w
	-- local y = (col - 1) / self.tiles.cols * sheet_h
	return (col - 1) / self.tiles.cols, (row - 1) / self.tiles.rows
end

function Terrain:getCenterXY()
	return self.cols * 0.5, self.rows * 0.5
end

function Terrain:updateMeshVertices()
	if self.must_update then
		self.model.mesh:setVertices(self.model.verts)
		self.must_update = false
	end
end

-- created so that calling the Terrain table (like Terrain({x = 50, y = 100}) or whatever), returns a new instance of Terrain
local TerrainMetatable = {
	__call = Terrain.new
}
setmetatable(Terrain, TerrainMetatable)

-- return class table
return Terrain