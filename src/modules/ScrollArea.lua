-- class table
local ScrollArea = {}

-- created so that instances of ScrollArea use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = ScrollArea
}

function ScrollArea.new(ScrollArea, settings)
	if settings then
		assert(type(settings) == 'table', "ScrollArea constructor requires argument to be settings table")
	end
	local self = settings
	setmetatable(self, instanceMetatable)
	
	--[[
		this class intends to be like a smarter scissor function
		player controls active scroll area with configurable controls
			action | default key
			-------------
			scroll up | mouse wheel up
			scroll down | mouse wheel down
			pan with mouse movement | hold right click and move mouse
	]]
	
	assert(self.draw_texture or self.draw_function, "New ScrollArea instance must be given either a .draw_texture or a .draw_function")
	if self.draw_texture then
		assert(self.draw_texture:typeOf("Drawable"), string.format("ScrollArea was given a value of type %s, expected type: Drawable", type(self.draw_texture)))
	end
	if self.draw_function then
		local df_type = type(self.draw_function)
		assert(df_type == 'function', string.format("ScrollArea was given a value of type %s, expected type: function", df_type))
	end
	
	return self
end

function ScrollArea:draw()
	-- set mask
	
	-- render draw texture/function
	
	-- unset mask and render scrollbars
end

-- created so that calling the ScrollArea table (like ScrollArea({x = 50, y = 100}) or whatever), returns a new instance of ScrollArea
local ScrollAreaMetatable = {
	__call = ScrollArea.new
}
setmetatable(ScrollArea, ScrollAreaMetatable)

-- return class table
return ScrollArea