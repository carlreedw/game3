-- class table
local Card = {}

local lg = love.graphics

-- created so that instances of Card use the class's functions (like :draw, if I had hadded that in this file)
local instanceMetatable = {
	__index = Card
}

function Card.new(Card, settings)
	if settings then
		assert(type(settings) == 'table', "Card constructor requires argument to be settings table")
	end
	local instance = settings
	local sw, sh = lg.getDimensions()
	instance.x = instance.x or sw * 0.5
	instance.y = instance.y or sh * 0.5
	instance.w = instance.w or 64
	instance.h = instance.h or 89
	instance.size = instance.size or 1
	
	setmetatable(instance, instanceMetatable)
	return instance
end

function Card:getBounds()
	local size = self.size
	return self.x, self.y, self.w * self.size, self.h * self.size
end

function Card:draw()
	local cx, cy, cw, ch = self:getBounds()
	local z = self.size
	lg.setColor(0, 0, 0)
	lg.rectangle('fill', cx - cw * 0.5, cy - ch * 0.5, cw, ch, 4*z, 4*z, 8)
	
	local mx, my = love.mouse.getPosition()
	if self:mouse_hover(mx, my) then
		lg.setColor(1, 1, 1)
	else
		lg.setColor(.4, .4, .4)
	end
	lg.setLineWidth(z * 0.5)
	lg.rectangle('line', cx - cw * 0.45, cy - ch * 0.465, cw * 0.9, ch * 0.465*2, 4*z, 4*z, 8)
end

function Card:mouse_hover(mx, my)
	local x, y, w, h = self:getBounds()
	if mx > x - w * 0.5 and mx < x + w * 0.5 and my > y - h * 0.5 and my < y + h * 0.5 then
		return true
	end
end

-- created so that calling the Card table (like Card({x = 50, y = 100}) or whatever), returns a new instance of Card
local CardMetatable = {
	__call = Card.new
}
setmetatable(Card, CardMetatable)

-- return class table
return Card