-- class table
local Console = {}

-- created so that instances of Console use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = Console
}

function Console.new(Console, settings)
	if settings then
		assert(type(settings) == 'table', "Console constructor requires argument to be settings table")
	end
	local self = settings
	setmetatable(self, instanceMetatable)
	
	
	
	return self
end

function Console:report()
	print('Console reporting - self.name = ' .. tostring(self.name))
end

-- created so that calling the Console table (like Console({x = 50, y = 100}) or whatever), returns a new instance of Console
local ConsoleMetatable = {
	__call = Console.new
}
setmetatable(Console, ConsoleMetatable)

-- return class table
return Console