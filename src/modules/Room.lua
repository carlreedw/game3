-- class table
local Room = {}

Room.level_data = {
	-- platforms = {
		-- -- 1
		-- {
		-- }
	-- }
}

-- created so that instances of Room use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = Room
}

function Room.new(Room, settings)
	if settings then
		assert(type(settings) == 'table', "Room constructor requires argument to be settings table")
	end
	local self = settings
	setmetatable(self, instanceMetatable)
	
	--[[
		
	]]
	
	self:load_level(self.level_index or 1)
	
	return self
end

function Room:draw()
	-- draw bg wall
	
	-- draw floors
	lg.setColor(.8, .8, .77, 1)
	lg.rectangle('fill', -200, 0, 400, 20)
	
	-- draw walls
	lg.setColor(.05, .18, .16, .85)
	lg.rectangle('fill', -200, 20, 400, game.h)
	lg.setColor(.15, .8, .6, 1)
	lg.rectangle('fill', -212, 0, 12, game.h)
	lg.rectangle('fill', 200-12, 0, 12, game.h)
	
	-- draw platforms
	lg.setColor(1, 1, 1)
	-- for i, platform in ipairs(self.platforms) do
		-- lg.line(unpack(platform))
	-- end
end

function Room:load_level(level_index)
	assert(type(level_index) == "number", "level_index should be type number, was: " .. type(level_index))
	
	
end

-- created so that calling the Room table (like Room({x = 50, y = 100}) or whatever), returns a new instance of Room
local RoomMetatable = {
	__call = Room.new
}
setmetatable(Room, RoomMetatable)

-- return class table
return Room