-- class table
local Background = {}

-- created so that instances of Background use the class's functions (like :draw, if I had hadded that in this file)
local instanceMetatable = {
	__index = Background
}

function Background.new(Background, settings)
	if settings then
		assert(type(settings) == 'table', "Background constructor requires argument to be settings table")
	end
	local self = settings
	
	
	
	setmetatable(self, instanceMetatable)
	return self
end

function Background:update(dt)
	
end

function Background:draw()
	self:drawStars()
end

function Background:drawStars()
	local alpha, x, y, rad
	for i = 1, 200 do
		alpha = love.math.noise(i * 24.284 + game.time*.18)
		lg.setColor(1, 1, 1, alpha)
		
		x = game.w * 1.5 * love.math.noise(i + .23958) - game.w * 0.25
		y = game.h * 1.5 * love.math.noise(-i + .23958) - game.h * 0.25
		rad = love.math.noise(i + 52359.235 + game.time*.14) * 4.5
		lg.circle('fill', x, y, rad, 12)
	end
end

-- created so that calling the Background table (like Background({x = 50, y = 100}) or whatever), returns a new instance of Background
local BackgroundMetatable = {
	__call = Background.new
}
setmetatable(Background, BackgroundMetatable)

-- return class table
return Background