-- class table
local Unit = {
	types = {},
	player_types = {"templar", "brute", "runner", "miner", "master"},
	enemy_types = {"boneman", "hobgob", "jell", "bonedisaster", "triffany"},
	sprite_coords = {
		templar = {
			x = 3,
			y = 2,
			w = 21,
			h = 38
		},
		brute = {
			x = 30,
			y = 3,
			w = 23,
			h = 37
		},
		runner = {
			x = 58,
			y = 3,
			w = 23,
			h = 37
		},
		miner = {
			x = 85,
			y = 2,
			w = 21,
			h = 37
		},
		master = {
			x = 110,
			y = 1,
			w = 23,
			h = 39
		},
		boneman = {
			x = 2,
			y = 44,
			w = 21,
			h = 36
		},
		hobgob = {
			x = 26,
			y = 42,
			w = 21,
			h = 38
		},
		jell = {
			x = 49,
			y = 62,
			w = 22,
			h = 18
		},
		bonedisaster = {
			x = 72,
			y = 42,
			w = 21,
			h = 38
		},
		triffany = {
			x = 95,
			y = 43,
			w = 21,
			h = 37
		}
	},
	actions_by_type = {
		templar = {
			"walk"
		},
		brute = {
			"walk"
		},
		runner = {
			"walk"
		},
		miner = {
			"walk"
		},
		master = {
			"walk"
		},
		boneman = {
			"walk"
		},
		hobgob = {
			"walk"
		},
		jell = {
			"walk"
		},
		bonedisaster = {
			"walk"
		},
		triffany = {
			"walk"
		}
	}
}
local atlas_width, atlas_height

function Unit.init(arg_table)
	-- Unit.sprite_atlas = assets.images.units
	Unit.sprite_atlas = arg_table.sprite_atlas
	atlas_width, atlas_height = Unit.sprite_atlas:getDimensions()
	for name, coords in pairs(Unit.sprite_coords) do
		table.insert(Unit.types, name)
		
		-- normalize each unit type's sprite atlas coords to be in the range [0, 1]
		coords.x = coords.x / atlas_width
		coords.w = coords.w / atlas_width
		coords.y = coords.y / atlas_height
		coords.h = coords.h / atlas_height
	end
end

-- created so that instances of Unit use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = Unit
}

function Unit.new(Unit, settings)
	if settings then
		assert(type(settings) == 'table', "Unit constructor requires argument to be settings table")
		
		-- determine instance.type_index and instance.atlas_coords
		settings.type_index = table.find(Unit.types, settings.type)
		assert(settings.type_index, "Unit constructor settings table needs a 'type' value that matches one from the Unit.types list - given settings.type: " .. tostring(settings.type))
		settings.sprite_coords = Unit.sprite_coords[settings.type]
		assert(settings.sprite_coords, "Unit constructor failed to assign sprite_coords to new Unit instance")
		
		-- create sprite and set texture and coords (based on prev determined type)
		settings.sprite = settings.sprite or modules.Sprite({})
		settings.sprite:setTexture(Unit.sprite_atlas)
		local u, v, w, h = settings.sprite_coords.x, settings.sprite_coords.y, settings.sprite_coords.w, settings.sprite_coords.h
		settings.sprite:setUVBounds(u, v, w, h)
	end
	local instance = settings
	setmetatable(instance, instanceMetatable)
	instance.scale = settings.scale or 0.5
	instance:stand(0, 0, 0)
	instance.row, instance.col = 0, 0
	instance:setControllerFacing(1, 0)
	instance.actions = instance.actions or {}
	for i, action_name in ipairs(instance.actions_by_type[instance.type]) do
		instance.actions[action_name] = modules.Action({
			type = action_name,
			unit = instance
		})
	end
	
	return instance
end

function Unit:draw()
	if self.sprite then
		self.sprite:draw()
	end
end

--[[
	player/cpu facing functions
--]]

function Unit:setControllerFacing(dx, dy)
	local model = self.sprite.model
	-- print(string.format("dx, dy: %s, %s", dx, dy))
	local z_rot = math.atan2(dx, dy)
	model:setTransform(nil, {-math.pi * 0.5, 0, -z_rot}, nil)
	-- if self == game.states[1].units[1] then
		-- print(string.format("dx, dy, z_rot: %s, %s, %s", dx, dy, z_rot))
	-- end
	self.controller_dx = dx
	self.controller_dy = dy
end

-- created so that calling the Unit table (like Unit({x = 50, y = 100}) or whatever), returns a new instance of Unit
local UnitMetatable = {
	__call = Unit.new
}
setmetatable(Unit, UnitMetatable)

function Unit:stand(x, y, z)
	local scale = 0.5
	local model = self.sprite.model
	local rx, ry, rz = model.rotation[1], model.rotation[2], model.rotation[3]
	model:setTransform(
		{x, y, z + scale*0.5 - 0.001},
		{rx, ry, rz},
		{scale, scale, scale}
	)
	self.row, self.col = math.ceil(x), math.ceil(y)
	-- if self == game.states[1].units[1] then
		-- print(string.format("player unit model:setTransform(.. rx = %s, ry = %s, rz = %s)", rx, ry, rz))
	-- end
end

function Unit:getPlayerTypes()
	return self.player_types
end

function Unit:getEnemyTypes()
	return self.enemy_types
end

function Unit:getPlayerIcon(size)
	return self.sprite:getVisibleTexture()
end

-- return class table
return Unit