-- class table
local Bart = {
	-- treat these as static class constants
	walk_speed_mult = 222,
	fart_force_mult = 455,
	grav_force_mult = 155,
	vx_threshold = 5,
	vy_threshold = 5,
	sprite_width = 32,
	sprite_height = 48,
	-- [0, 1], 0 is no change, 1 is instant acceleration
	x_accel_lerp_factor = .5,
	y_accel_lerp_factor = .05
}

-- created so that instances of Bart use the class's functions (like :draw, if I had added that in this file)
local instanceMetatable = {
	__index = Bart
}

function Bart.new(Bart, settings)
	if settings then
		assert(type(settings) == 'table', "Bart constructor requires argument to be settings table")
	end
	local self = settings
	setmetatable(self, instanceMetatable)
	
	-- initialize non-static properties
	self.x = self.x or 0
	self.y = self.y or 0
	self.vx, self.vy = 0, 0
	
	self.bowels = {
		fart_current = 0,
		fart_max = 50
	}
	
	debug.info.bart = self
	
	return self
end

function Bart:update(dt)
	-- compute forces
	-- self.fx, self.fy = 0, self.mass * -9.81 * self.grav_force_mult
	
	
	local target_vx = 0
	local target_vy = -self.grav_force_mult
	
	-- apply fart power
	if lk.isDown('space') then
		local fart_power_spent = self:spend_fart(dt)
		-- target_vy = fart_power_spent * self.fart_force_mult
		target_vy = self.grav_force_mult
	else
		self:build_fart(dt)
	end
	
	-- move left r
	if lk.isDown('a') and not lk.isDown('d') then
		target_vx = -self.walk_speed_mult
	elseif lk.isDown('d') and not lk.isDown('a') then
		target_vx = self.walk_speed_mult
	end
	-- if lk.isDown('w') then
		-- target_vy = self.grav_force_mult
	-- end
	
	-- update velocities and positions
	-- self.ax = self.fx / self.mass
	-- self.ay = self.fy / self.mass
	-- self.vx = self.vx + self.ax * dt
	
	self.vx = self.x_accel_lerp_factor * target_vx + (1 - self.x_accel_lerp_factor) * self.vx
	if math.abs(self.vx) < self.vx_threshold then
		self.vx = 0
	end
	-- self.vy = self.vy + self.ay * dt
	self.vy = self.y_accel_lerp_factor * target_vy + (1 - self.y_accel_lerp_factor) * self.vy
	if math.abs(self.vy) < self.vy_threshold then
		self.vy = 0
	end
	self.x = self.x + self.vx * dt
	self.y = self.y + self.vy * dt
	
	if self.y < 20 then
		self.y = 20
		self.vy = math.abs(self.vy)
	end
end

function Bart:type()
	return "Bart"
end

function Bart:spend_fart(dt)
	local fart_power = self.bowels.fart_current
	if fart_power <= 0 then
		self:shart()
	else
		local new_fart_power = math.max(fart_power - 22 * dt, 0)
		local fart_power_spent = fart_power - new_fart_power
		self.bowels.fart_current = new_fart_power
		
		return fart_power_spent
	end
	return 0
end

function Bart:build_fart(dt)
	local fart_power = self.bowels.fart_current
	if fart_power < self.bowels.fart_max then
		self.bowels.fart_current = math.min(fart_power + 16.666 * dt, self.bowels.fart_max)
	end
end

function Bart:shart()
	
end

function Bart:draw()
	lg.setColor(1, 1, 1)
	local x, y, w, h = self.x, self.y, self.sprite_width, self.sprite_height
	lg.rectangle('fill', x - w/2, y, w, h)
end

function Bart:draw_ui()
	-- draw fart meter
	local fill = self.bowels.fart_current / self.bowels.fart_max
	local fill_chunks = math.ceil(fill * 5)
	
	-- draw outline
	lg.setColor(.828, .87, .58)
	local sw, sh = game.w, game.h
	local x, y, w, h = sw * 0.5 + 220, sh * 0.765, sw * 0.05, sh * (1 - 0.765 - 0.025)
	lg.rectangle('line',  x, y, w, h)
	
	-- draw blocks
	local block_w, block_h = w*0.9, h * 0.8 / 5
	local y_buffer = h * 0.2 / 10
	local x_buffer = w * 0.05
	for i = 1, fill_chunks do
		-- draw fill_chunk
		local partial_block = i == fill_chunks and fill ~= 1
		if partial_block then
			lg.setColor(.828, .87, .58, .35)
			lg.rectangle('fill', x + x_buffer, y + h - y_buffer - (i-1) * (2 * y_buffer + block_h), block_w, -block_h)
			
			lg.setColor(.888, .89, .78, 1)
			local partial_block_height = fill * 5 - math.floor(fill * 5)
			lg.rectangle('fill', x + x_buffer, y + h - y_buffer - (i-1) * (2 * y_buffer + block_h), block_w, -block_h * partial_block_height)
		else
			lg.setColor(.828, .87, .58)
			lg.rectangle('fill', x + x_buffer, y + h - y_buffer - (i-1) * (2 * y_buffer + block_h), block_w, -block_h)
		end
	end
end

function Bart:listen(bool)
	if bool then
		if not self.events then
			self.events = {}
			-- self.events.kp = events("keypressed", function(key, scancode, isrepeat)
				
			-- end)
			-- self.events.mm = events("mousemoved", function(x, y, dx, dy)
				
			-- end)
		end
		
		for i, e in pairs(self.events) do
			e:register()
		end
	else
		
		for i, e in pairs(self.events) do
			e:remove()
		end
	end
end

-- created so that calling the Bart table (like Bart({x = 50, y = 100}) or whatever), returns a new instance of Bart
local BartMetatable = {
	__call = Bart.new
}
setmetatable(Bart, BartMetatable)

-- return class table
return Bart