--[[
PROJECT: prototype
AUTHORS: daddy boy
DATE: 2022-09-27
DESC: hero game co game prototype
ABLE: "D:/love2d/11.4/love.exe" "C:/dev/game3/src"
SIK4ULAH: "C:/dev/love/11.4/love.exe" "C:/dev/game3/src"
--]]

function love.load()
	lg = love.graphics
	game = love.filesystem.load('game.lua')({
		run_tests = true,
		-- load_logging = true
	})
	
	game.events.set_wh_on_resize = events('resize', function(w, h)
		game.w, game.h = w, h
	end)
	game.events.set_wh_on_resize.callback(lg.getDimensions())
	
	-- enable key repeat
	lk.setKeyRepeat(true)
	
	-- enable quit on esc and debug.debug when press debug.debug_key (f10 default)
	debug.listen(true)
	game:listen(true)
	
	game:start()
end

function love.update(dt)
	game:update(dt)
end

function love.draw()
	game:draw()
end