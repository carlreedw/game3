local state = {}

function state:init()
	class = modules.Class({name = 'ABC'})
	class:report()
end

function state:draw()
	local lg = love.graphics
	lg.setColor(1, 1, 1)
	lg.print('hi', 20, 20)
end

return state