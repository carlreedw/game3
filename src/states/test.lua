local state = {}

function state:init()
	state.terrain = modules.Terrain({heights = {1}})
	
	g3d.camera.lookAt(4, 4, 4, 4, 4, 3)
end

function state:listen(bool)
	if bool then
		if not state.events then
			state.events = {}
			state.events.kp = events("keypressed", function(key, scancode, isrepeat)
				if key == 'q' then
					g3d.camera.lookAt(4, 4, 4, 4, 4, 3)
				end
				if key == 'z' then
					debug.write_log("g3d_camera_inspection", inspect(g3d.camera))
				end
			end)
			state.events.mm = events("mousemoved", function(x, y, dx, dy)
				g3d.camera.firstPersonLook(dx, dy)
			end)
		end
		for i, e in pairs(state.events) do
			e:register()
		end
	else
		
		for i, e in pairs(state.events) do
			e:remove()
		end
	end
end

function state:update(dt)
	g3d.camera.firstPersonMovement(dt * 0.5)
end

function state:draw()
	state.terrain:draw()
	debug.report({1, 1, 1})
end

return state