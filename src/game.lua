----------------------------
-- global game object file
----------------------------
local args = ...
game = {}
game.time = 0
game.tests = {}
game.states = {}
game.shaders = {}
game.events = {}
-- game.tweens = {}
-- game.clocks = {}
game.colors = {}

--[[ setting in_debug_mode has the following effects:
	* adds temp_log_files directory in save data folder
]]
-- game.in_debug_mode = false
game.in_debug_mode = true

game.strict = true
game.vertexFormat = {
	{"VertexPosition", "float", 3},
	{"VertexTexCoord", "float", 2},
	{"VertexNormal", "float", 3},
	{"VertexColor", "byte", 4}
}

function game:start()
	game:push(states.test)
end

-- ----------------------------
-- -- per-frame functions
-- ----------------------------

function game:draw()
	-- draw each state, starting at end
	for i, state in ipairs(self.states) do
		if state.draw then
			state:draw()
		end
	end
end

function game:update(dt)
	self.time = self.time + dt
	game.mouseX, game.mouseY = love.mouse.getPosition()
	
	-- for name, clock in pairs(self.clocks) do
		-- clock:advance(dt)
		-- if clock.done then
			-- self.clocks[name] = nil
		-- end
	-- end
	
	-- for name, tween in pairs(self.tweens) do
		-- tween:update(dt)
		-- if tween.done then
			-- self.tweens[name] = nil
		-- end
	-- end
	
	-- update states, starting at bottom
	for i, state in ipairs(self.states) do
		if state.update then
			state:update(dt)
		end
	end
	
	game.lastMouseX, game.lastMouseY = game.mouseX, game.mouseY
end

-- -----------------------------
-- -- state functions
-- -----------------------------

function game:pop()
	local topstate = game.states[#game.states]
	if topstate.listen then
		topstate:listen(false)
	end
	if topstate.release then
		topstate:release()
	end
	game.states[#game.states] = nil
	local nextstate = game.states[#game.states]
	if nextstate then
		nextstate:listen(true)
	end
	collectgarbage()
end

function game:push(state, ...)
	local topstate = game.states[#game.states]
	if topstate and topstate.listen then
		topstate:listen(false)
	end
	table.insert(game.states, state)
	if state.init then state:init(...) end
	if state.listen then state:listen(true) end
end

-- -----------------------------
-- -- input functions
-- -----------------------------

function game:getMousePosition()
	return game.mouseX, game.mouseY
end

function game:listen(bool)
	if bool then
		for name, event in pairs(self.events) do
			event:register()
		end
	else
		for name, event in pairs(self.events) do
			event:remove()
		end
	end
end

-- -----------------------------
-- -- load global assets
-- -----------------------------

lg.setDefaultFilter("nearest")
assets = {}
assets.images = {}
assets.images.tileset2 = lg.newImage("images/tileset2.png")
assets.images.units = lg.newImage("images/units.png")

-- -----------------------------
-- -- load library, extend, module, and state files recurseively
-- -----------------------------

	--[[
		strategy: extend love.filesystem to have our recurseFiles function
		use recurseFiles to recurse through:
			/libraries
			/extends
			/modules
			/states
		in that order, adding each item as it is encountered
	]]

-- extend love.filesystem with our recurseFiles function
love.filesystem.recurseFiles = function(path, callback, ...)
	local results = {}
	for i, fname in ipairs(love.filesystem.getDirectoryItems(path)) do
		local fullpath = path .. '/' .. fname
		local info = love.filesystem.getInfo(fullpath)
		if info.type == 'directory' then
			results[fullpath] = love.filesystem.recurseFiles(fullpath, callback, ...)
		else
			local name = fname:match("([^/]+)%..+$")
			local ext = fname:match("[^.]+$")
			results[fullpath] = callback(fullpath, name, ext, ...)
		end
	end
	if next(results) then return results end
end

local recurse = love.filesystem.recurseFiles

-- create global libraries table and load in /libraries files (and all subdirectories etc recursively)
-- libraries are also stored in global var of their name
libraries = {}
recurse('libraries', function(fullpath, name)
	if args.load_logging then print('loading library ' .. name) end
	_G[name] = love.filesystem.load(fullpath)(fullpath)
	table.insert(libraries, _G[name])
end)

-- do similar for /extends files, but no global var/table registry
recurse('extends', function(fullpath, name)
	if args.load_logging then print('loading extend file ' .. name) end
	love.filesystem.load(fullpath)(fullpath)
end)

-- do similar for /modules
modules = {}
recurse('modules', function(fullpath, name)
	if args.load_logging then print('loading module ' .. name) end
	local mod = love.filesystem.load(fullpath)(fullpath)
	modules[name] = mod
end)

-- do similar for /states
states = {}
recurse('states', function(fullpath, name)
	if args.load_logging then print('loading state file ' .. name) end
	local state = love.filesystem.load(fullpath)(fullpath)
	states[name] = state
end)

-- initialize modules
modules.Terrain.init({tile_sheet = assets.images.tileset2})
modules.Unit.init({sprite_atlas = assets.images.units})

-----------------------------
-- make log file, random seed, and optionally run tests
-----------------------------

love.filesystem.remove('log.txt')
love.filesystem.newFile('log.txt')
debug.log("starting log:")

local rs = os.time()
game.rng = love.math.newRandomGenerator(rs)
debug.log("game.rng created with random seed: " .. tostring(rs))

-- aliases
lg = love.graphics
lk = love.keyboard
lm = love.mouse
lp = love.physics

-- g3d stuff
g3d = require("g3d/g3d")
io.stdout:setvbuf("no")

if args.run_tests then
	-- run all tests in the tests folder, compiling csv report to save to path
	local report = ''
	local separator = '|'
	local failures = 0
	
	local runTestSuite = function(path, name, ext)
		if ext ~= 'lua' then return end
		local success, suite = pcall(love.filesystem.load(path))
		
		-- make sure we have an actual test suite
		if not success then
			report = report .. "Encountered error when trying to open test suite file at path: " .. path .. "\n" .. "Error: " .. suite .. "\n\n"
		end
		if not suite or not suite.tests then
			report = report .. "File opened but no valid test suite returned from " .. name .. " in path: " .. path .. "\n\n"
			return
		end
		
		report = report .. "PATH" .. separator .. "SUITE\n"
		report = report .. path .. separator .. (suite.desc or "NO DESCRIPTION") .. "\n"
		report = report .. "RESULT" .. separator .. "TIME" .. separator .. "TEST" .. separator .. "NOTE\n"
		for i, test in ipairs(suite.tests) do
			local t0 = love.timer.getTime()
			local ok, err = pcall(test.run)
			local time = love.timer.getTime() - t0
			
			report = report ..
				string.format(	"%s" .. separator .. 		"%.4f" .. separator .. 	"%s" .. separator .. 	"%s\n",
								ok and "PASS" or "FAIL", 	time, 					test.desc, 				not ok and err or '')
			
			failures = failures + (ok and 0 or 1)
		end
		report = report .. "\n"
	end
	
	-- profiling and test execution
	local t0 = love.timer.getTime()
	love.filesystem.recurseFiles('tests', runTestSuite)
	local t1 = love.timer.getTime()
	
	-- clip trailing newline
	report = report:sub(1,-3)
	
	-- save to file
	local report_path = 'all_tests.csv'
	love.filesystem.write(report_path, report)
	
	-- print summary
	print(string.format("ran tests in %.4fs, printing report to " .. report_path, t1 - t0))
	print(tostring(failures) .. ' tests failed')
end

----
-- events hooks
----
local hooks = {
	'keypressed',
	'keyreleased',
	'mousepressed',
	'mousereleased',
	'wheelmoved',
	'mousemoved',
	'touchmoved',
	'touchpressed',
	'touchreleased',
	'focus',
	'resize',
	'quit',
	'textinput',
	'gamepadpressed',
	'gamepadreleased',
	'gamepadaxis',
	'joystickadded',
	'joystickpressed',
	'joystickreleased',
	'joystickremoved',
	'filedropped',
	'directorydropped',
}
events:hook(love, hooks)

return game

