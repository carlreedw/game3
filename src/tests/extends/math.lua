local args = ...

local suite = {
	desc = "Tests for the math.lua extend modules",
	tests = {
		{
			desc = "math.clamp(x, min, max) returns x clamped between min and max",
			run = function()
				local clamped1 = math.clamp(5, 6, 10)
				local clamped2 = math.clamp(7.5, 6, 10)
				local clamped3 = math.clamp(11, 6, 10)
				assert(clamped1 == 6, "5 not clamped up to 6 -- clamped1: " .. tostring(clamped1))
				assert(clamped2 == 7.5, "7.5 illegally clamped -- clamped2: " .. tostring(clamped2))
				assert(clamped3 == 10, "11 not clamped down to 10 -- clamped3: " .. tostring(clamped3))
			end
		},
		{
			desc = "math.round(x [, decimal_places]) returns x rounded to decimal_places (or integer if no 2nd arg). Rounds away from 0 if half-way between round targets.",
			run = function()
				local r1 = math.round(2.5)
				local r2 = math.round(2.3)
				local r3 = math.round(-2.65, 1)
				local r4 = math.round(295.6539, 3)
				assert(r1 == 3, "r1: " .. tostring(r1) .. " -- expected r1 == 3")
				assert(r2 == 2, "r2: " .. tostring(r2) .. " -- expected r2 == 2")
				assert(r3 == -2.7, "r3: " .. tostring(r3) .. " -- expected r3 == -2.7")
				assert(r4 == 295.654, "r4: " .. tostring(r4) .. " -- expected r4 == 295.654")
			end
		},
		{
			desc = "math.sign(x) returns 1, 0, or -1 if x is positive, 0, or negative.",
			run = function()
				local r1 = math.sign(0.0001)
				local r2 = math.sign(0)
				local r3 = math.sign(-25)
				assert(r1 == 1, "r1: " .. tostring(r1) .. " -- expected r1 == 1")
				assert(r2 == 0, "r2: " .. tostring(r2) .. " -- expected r2 == 0")
				assert(r3 == -1, "r3: " .. tostring(r3) .. " -- expected r3 == -1")
			end
		},
		{
			desc = "math.lerp(a, b, x) returns a interpolated to b by x amount (result NOT clamped)",
			run = function()
				local r1 = math.lerp(100, 200, 0.5)
				local r2 = math.lerp(1, 200, 0)
				local r3 = math.lerp(-5, 5, -2)
				assert(r1 == 150, "r1: " .. tostring(r1) .. " -- expected r1 == 150")
				assert(r2 == 1, "r2: " .. tostring(r2) .. " -- expected r2 == 1")
				assert(r3 == -25, "r3: " .. tostring(r3) .. " -- expected r3 == -25")
			end
		},
		{
			desc = "math.smooth(a, b, x) returns a cubic interpolated to b by x amount (results ARE clamped to a, b)",
			run = function()
				local r1 = math.smooth(100, 200, 0.5)
				local r2 = math.smooth(1, 200, 0)
				local r3 = math.smooth(-5, 5, 2)
				local r4 = math.smooth(0, 1, 0.1)
				assert(r1 == 150, "r1: " .. tostring(r1) .. " -- expected r1 == 150")
				assert(r2 == 1, "r2: " .. tostring(r2) .. " -- expected r2 == 1")
				assert(r3 == 5, "r3: " .. tostring(r3) .. " -- expected r3 == 5")
				assert(tostring(r4) == "0.028", "r4: " .. tostring(r4) .. " -- expected r4 == 0.028")
			end
		},
	}
}

return suite